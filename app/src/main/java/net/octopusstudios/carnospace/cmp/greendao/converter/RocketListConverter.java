package net.octopusstudios.carnospace.cmp.greendao.converter;

import net.octopusstudios.carnospace.cmp.pojo.Rocket;
import org.greenrobot.greendao.converter.PropertyConverter;


/**
 * Converts an array of Rocket enum into a String for easier DB saving
 * And then converts it back of course.
 * Since it's an enum i don't expect troubles other than backwards compatibility
 *
 * Created by Need2Revolt on 04/06/2017.
 */

public class RocketListConverter implements PropertyConverter<Rocket[], String> {

    public static final String SEPARATOR = ";---;";

    @Override
    public Rocket[] convertToEntityProperty(String databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        else {
            String[] list = databaseValue.split(SEPARATOR);
            Rocket[] availableRockets = new Rocket[list.length];
            int i = 0;
            for(String rocket : list) {
                availableRockets[i] = Rocket.valueOf(rocket);
                i++;
            }
            return availableRockets;
        }
    }

    @Override
    public String convertToDatabaseValue(Rocket[] entityProperty) {
        if(entityProperty==null){
            return null;
        }
        else{
            StringBuilder sb= new StringBuilder();
            for(Rocket link : entityProperty){
                sb.append(link.name());
                sb.append(SEPARATOR);
            }
            return sb.toString();
        }
    }
}
