/**
 * This code is part of "LEMP: Leaving Earth Mission Planner" android application.
 * Released under GPL v3
 * Written by Need2Revolt (francesco.davide.carnovale@gmail.com)
 */
package net.octopusstudios.carnospace.cmp.listener;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import net.octopusstudios.carnospace.cmp.R;
import net.octopusstudios.carnospace.cmp.adapter.MissionsAdapter;
import net.octopusstudios.carnospace.cmp.pojo.DaoSession;
import net.octopusstudios.carnospace.cmp.pojo.Mission;
import net.octopusstudios.carnospace.cmp.pojo.Rocket;
import net.octopusstudios.carnospace.cmp.pojo.Stage;
import net.octopusstudios.carnospace.cmp.status.SharedState;

import java.util.ArrayList;
import java.util.List;

/**
 * Class in charge to open a popup view with mission information and collect user input
 * to create the actual mission item.
 *
 * Created by Need2Revolt on 16/02/2017.
 */
public class AddMissionListener implements View.OnClickListener {

    private Context ctx;
    private List<Mission> missions;
    private MissionsAdapter missionsAdapter;
    private DaoSession daoSession;

    public AddMissionListener(Context ctx, List<Mission> missions, MissionsAdapter missionsAdapter) {
        this.ctx = ctx;
        this.missions = missions;
        this.missionsAdapter = missionsAdapter;
        daoSession = ((SharedState)ctx.getApplicationContext()).getDaoSession();
    }

    /**
     * all the logic is here, basically class description fits here too
     */
    @Override
    public void onClick(View view) {

        LayoutInflater li = LayoutInflater.from(ctx);
        View promptsView = li.inflate(R.layout.mission_input_dialog, null);
        final TextView stageNameEdit = (TextView) promptsView.findViewById(R.id.missionNameEdit);
        final CheckBox junoCheckbox = (CheckBox) promptsView.findViewById(R.id.junoCheckbox);
        final CheckBox atlasCheckbox = (CheckBox) promptsView.findViewById(R.id.atlasCheckbox);
        final CheckBox soyuzCheckbox = (CheckBox) promptsView.findViewById(R.id.soyuzCheckbox);
        final CheckBox saturnCheckbox = (CheckBox) promptsView.findViewById(R.id.saturnCheckbox);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);

        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = stageNameEdit.getText().toString();
                                //this is not really elegant, but i'm too tired...
                                List<Rocket> rocketsList = new ArrayList<Rocket>(4);
                                if(junoCheckbox.isChecked()) {
                                    rocketsList.add(Rocket.JUNO);
                                }
                                if(atlasCheckbox.isChecked()) {
                                    rocketsList.add(Rocket.ATLAS);
                                }
                                if(soyuzCheckbox.isChecked()) {
                                    rocketsList.add(Rocket.SOYUZ);
                                }
                                if(saturnCheckbox.isChecked()) {
                                    rocketsList.add(Rocket.SATURN);
                                }

                                //if user selected none, we assume it has them all
                                Rocket [] availableRockets;
                                if(rocketsList.size() >= 0) {
                                    availableRockets = rocketsList.toArray(new Rocket[0]);
                                }
                                else {
                                    availableRockets = Stage.allRockets;
                                }

                                Mission m = new Mission(name, availableRockets);
                                missions.add(0, m);
                                daoSession.insertOrReplace(m);
                                missionsAdapter.notifyDataSetChanged();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
