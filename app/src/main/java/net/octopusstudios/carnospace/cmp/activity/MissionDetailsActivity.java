/**
 * This code is part of "LEMP: Leaving Earth Mission Planner" android application.
 * Released under GPL v3
 * Written by Need2Revolt (francesco.davide.carnovale@gmail.com)
 */
package net.octopusstudios.carnospace.cmp.activity;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.octopusstudios.carnospace.cmp.R;
import net.octopusstudios.carnospace.cmp.adapter.StagesAdapter;
import net.octopusstudios.carnospace.cmp.listener.AddStageListener;
import net.octopusstudios.carnospace.cmp.pojo.DaoSession;
import net.octopusstudios.carnospace.cmp.pojo.Mission;
import net.octopusstudios.carnospace.cmp.pojo.Stage;
import net.octopusstudios.carnospace.cmp.status.SharedState;

/**
 * This is the second and last GUI screen you see in the app.
 * It's basically an editable list of stages your mission is composed of.
 *
 * It's some android mambo-jumbo, which i'm not really sure how it works =)
 *
 * Created by Need2Revolt on 12/02/2017.
 */
public class MissionDetailsActivity extends AbstractMissionPlannerMenuAwareActivity {

    private StagesAdapter stagesAdapter;
    private Mission mission;
    private DaoSession daoSession;

    /**
     * Init some variables and some listeners and some adapters...
     * basically glue things uup toghether so that they work afterwards
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //init laayout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_details);
        //adds toolbar actions (i.e. help + about)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //init some internal state variables
        SharedState sharedState = (SharedState) getApplicationContext();
        mission = sharedState.getSelectedMission();
        daoSession = sharedState.getDaoSession();

        final Context ctx = this;
        //the view holding the stages list
        ListView stagesList = (ListView) findViewById(R.id.missionsListView);
        stagesAdapter = new StagesAdapter(this, mission.getMissionStages());

        //adds stages adapter
        stagesList.setAdapter(stagesAdapter);
        stagesAdapter.notifyDataSetChanged();

        //add context menu
        registerForContextMenu(stagesList);

        //init add stage button and it's listener
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        AddStageListener addStageListener = new AddStageListener(stagesAdapter, mission, ctx, stagesList);
        fab.setOnClickListener(addStageListener);
    }

    /**
     * Not really sure about this one... it's been a while between coding and documenting...
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.missionsListView) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.mission_details_menu, menu);
        }
    }

    /**
     * Handles the context menu actions, so far only delete is avilable.
     *
     * @param item the item selected by the user
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {

            case R.id.delete_stage:
                Stage toDelete = mission.getMissionStages().remove(info.position);
                mission.removeStageCost(toDelete.getTotalCost());
                daoSession.delete(toDelete);
                daoSession.insertOrReplace(mission);
                stagesAdapter.notifyDataSetChanged();
                //TODO stage mass and rockets recalculation logic?
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     * @param menu
     * @return always true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_missions_lister, menu);
        return true;
    }

    /**
     * Not sure this is even needed
     */
    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * Not sure this is even needed
     */
    @Override
    public void onStop() {
        super.onStop();
    }

}
